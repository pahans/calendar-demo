import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import db, {getEvents} from './db';
import initNotifications from './notifications';

ReactDOM.render(<App />, document.getElementById('root'));

getEvents().toArray().then((events)=>{
    initNotifications(db, events);
});
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
