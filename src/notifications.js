const pendingNotifications = {};

export default function initNotifications(db, events = []) {
    events.forEach((event) => createNotification(event.id, event));
    db.events.hook("updating", updateNotification);
    db.events.hook("creating", createNotification);
    db.events.hook("deleting", deleteNotification);
}

function deleteNotification(primKey) {
    clearTimeout(pendingNotifications[primKey]);
}
function updateNotification(mods, primKey, event, trans) {
    clearTimeout(pendingNotifications[primKey]);
    const timeDiff = new Date(event.startDate).getTime() - Date.now();
    if (timeDiff < 0) {
        return;
    }
    pendingNotifications[primKey] = setTimeout(() => {
        showNotification(`Reminder on event : ${event.subject}`);
    }, timeDiff);
}
function createNotification(primKey, event, trans) {
    const timeDiff =  new Date(event.startDate).getTime() - Date.now();
    if (timeDiff < 0) {
        return;
    }
    pendingNotifications[event.id] = setTimeout(() => {
        showNotification(`Reminder on event : ${event.subject}`);
    }, timeDiff);
}

function showNotification(title) {
    Notification.requestPermission(function (permission) {
        // If the user accepts, let's create a notification
        if (permission === "granted") {
            new Notification(title);
        }
    });
}