import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import EditEvent from './EditEvent';
import Calendar from './Calendar';
import { getEvents } from './db';

class Home extends Component {
    constructor() {
        super();
        this.state = {
            events: {},
            startDate: new Date()
        };
        this.onExport = this.onExport.bind(this);
        this.onNextMonth = this.onNextMonth.bind(this);
        this.onPreviousMonth = this.onPreviousMonth.bind(this);
    }
    onPreviousMonth() {
        const { startDate } = this.state;
        this.setState({
            startDate: new Date(startDate.setMonth(startDate.getMonth() - 1))
        });
    }
    onNextMonth() {
        const { startDate } = this.state;
        this.setState({
            startDate: new Date(startDate.setMonth(startDate.getMonth() + 1))
        });
    }
    onExport() {
        let toExport = "Subject,Start Date,Start Time,End Date,End Time,Location,Description";

        getEvents().toArray().then((eventsArray) => {

            eventsArray.forEach((event) => {
                toExport += "\n";
                const startDate = new Date(event.startDate);
                const endDate = new Date(event.startDate);
                const data = [
                    event.subject,
                    startDate.toLocaleDateString(),
                    startDate.toLocaleTimeString(),
                    endDate.toLocaleDateString(),
                    endDate.toLocaleTimeString(),
                    event.location,
                    event.description
                ];
                toExport += data.join(",");
            });
            const fileName = "export.csv";
            const data = encodeURI("data:text/csv;charset=utf-8," + toExport);
            let link = document.createElement('a');
            link.setAttribute('href', data);
            link.setAttribute('download', fileName);
            link.click();
        });

    }
    componentDidMount() {
        const events = {};
        getEvents().toArray().then((eventsArray) => {
            eventsArray.forEach((event) => {
                const startDate = new Date(event.startDate);
                const key = `${startDate.getFullYear()}-${startDate.getMonth()}-${startDate.getDate()}`;
                events[key] = events[key] || [];
                events[key].push(event);
                this.setState({
                    events,
                });
            });
        });
    }
    render() {
        const { startDate } = this.state;
        return (
            <div>
                <button onClick={this.onExport}>Export</button>
                <button onClick={this.onPreviousMonth}>&lt;</button>
                {startDate.toLocaleString("en-us", {
                    month: "long",
                    year: "numeric"
                })}
                <button onClick={this.onNextMonth}>&gt;</button>
                <Calendar events={this.state.events} year={startDate.getFullYear()} month={startDate.getMonth()} />
            </div>
        );
    }

}
class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <div>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/edit-event/:id?" component={EditEvent} />
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;
