import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './Calendar.css'

const DAYS_PER_PAGE = 35;

export default class Calendar extends Component {
    constructor() {
        super();
        this.renderDay = this.renderDay.bind(this);
    }
    renderDay(date) {
        const { events } = this.props;

        const eventsForDay = events[`${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`] || {};
        return (
            <div className="calendar-item">
                <div>
                    {date.toLocaleString("en-us", {
                        month: "short",
                        weekday: "short",
                        day: "numeric"
                    })}
                </div>
                <div>
                    <ul>
                        {
                            Object.keys(eventsForDay).map((event) => {
                                return (<li>
                                    <Link to={`/edit-event/${eventsForDay[event].id}`}>
                                        {
                                            eventsForDay[event].startDate.toLocaleTimeString("en-us", {
                                                hour: "numeric",
                                                minute: "numeric",
                                            })
                                        }
                                        -
                                        {
                                            eventsForDay[event].subject
                                        }

                                    </Link></li>)
                            })
                        }
                    </ul>
                </div>

            </div>
        );
    }
    fillMonth() {
        const { month, year } = this.props;
        const firstDayOfMonth = new Date(year, month, 1);
        const day = firstDayOfMonth.getDay();

        const monthDays = [];

        // construct first week of the month
        for (let i = 0; i <= day; i++) {
            const date = new Date(year, month, 1 - i);
            monthDays.unshift(date);
        }

        // fill rest
        for (let j = 1; monthDays.length < DAYS_PER_PAGE; j++) {
            const date = new Date(year, month, 1 + j);
            monthDays.push(date);
        }
        return monthDays;
    }

    render() {
        return (
            <div className="calendar-container">
                {
                    this.fillMonth().map(this.renderDay)
                }
                <button className="btn red add-event" type="button">
                    <Link to="/edit-event"><span>+</span></Link>
                </button>
            </div>
        );
    }
}

Calendar.defaultProps = {
    month: new Date().getMonth(),
    year: new Date().getFullYear(),
};