import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import './EditEvent.css';
import { putEvent, getEvent, updateEvent, deleteEvent } from './db';
import { Link } from 'react-router-dom';


export default class EditEvent extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
        this.onChangeSubject = this.onChangeSubject.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeLocation = this.onChangeLocation.bind(this);
        this.state = {
            subject: "",
            startDate: null,
            endDate: null,
            location: "",
            description: "",
            id: undefined,
        };
    }
    handleStartDateChange(value) {
        this.setState({
            startDate: value,
        });
    }
    handleEndDateChange(value) {
        this.setState({
            endDate: value,
        });
    }
    onChangeLocation(e) {
        const value = e.target.value;
        this.setState({
            location: value,
        });
    }
    onChangeDescription(e) {
        const value = e.target.value;
        this.setState({
            description: value,
        });
    }
    onChangeSubject(e) {
        const value = e.target.value;
        this.setState({
            subject: value,
        });
    }
    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.subject.trim()) {
            alert("Please specify a subject for your event");
        }
        if (!this.state.startDate) {
            alert("Please specify a start date");
        }
        const event = {
            subject: this.state.subject,
            startDate: this.state.startDate,
            endDate: this.state.endDate,
            location: this.state.location,
            description: this.state.description,
        }
        const id = this.props.match.params.id;
        if (id) {
            updateEvent(parseInt(id), event);
        } else {
            putEvent(event);
        }
        this.props.history.push('/')
    }

    componentDidMount() {
        if (!this.props.match.params.id) {
            return;
        }
        getEvent(this.props.match.params.id).then((event) => {
            this.setState({
                ...event
            });
        });

    }
    render() {

        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <h2>{this.props.match.params.id ? 'Edit' : 'Add'} event</h2>
                    <div>
                        <label>Subject:</label>
                        <input type="text" value={this.state.subject} onChange={this.onChangeSubject} />
                    </div>
                    <div>
                        <label>Start date:</label>
                        <DatePicker
                            selected={this.state.startDate}
                            onChange={this.handleStartDateChange}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                        />
                    </div>
                    <div>
                        <label>End date:</label>
                        <DatePicker
                            selected={this.state.endDate}
                            onChange={this.handleEndDateChange}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                        />
                    </div>
                    <div>
                        <label>Location:</label>
                        <input type="text" value={this.state.location} onChange={this.onChangeLocation} />
                    </div>
                    <div>
                        <label>Description:</label>
                        <textarea value={this.state.description} onChange={this.onChangeDescription} />
                    </div>

                    <div className="button">
                        <Link to="/">Back</Link >
                        {this.props.match.params.id ? 
                            <button onClick={()=> deleteEvent(this.props.match.params.id)}>Delete</button>
                            : null}
                        <button type="submit">Save</button>
                    </div>
                </form>
            </div>
        );
    }
}