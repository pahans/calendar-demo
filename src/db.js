import Dexie from 'dexie';

const db = new Dexie('calendarDb');

db.version(1).stores({
    events: '++id, subject, startDate, endDate, location, description'
});

export function putEvent(event) {
    return db.events.put(event);
}

export function updateEvent(id, event) {
    return db.events.update(id, event);
}

export function getEvents() {
    return db.events;
}

export function deleteEvent(id) {
    return db.events.where("id").equals(parseInt(id)).delete();
}

export function getEvent(id) {
    return db.events.where("id").equals(parseInt(id)).first();
}

export default db;